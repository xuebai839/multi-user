=========
Community
=========

Discord
=======

Feel free to join our `discord server <https://discord.gg/aBPvGws>`_ ! 

You will find help, a way to take part in the project, public collaborative sessions, people to create with, information about the addon's progress and much more.

.. TODO: Make a call on discord get a link / image for evry contributor ?

Contributors
============

Swann, Fabian, NotFood, Poochyc, Valentin, Adrien, Tanguy, Bruno, Gorgio, Axel, Ultr-X, Wuaieyo, Softyoda, Staz, Ikxi, Kysios.